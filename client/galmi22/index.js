

const main = () => 
{
    const decrypt = (hash, password) => CryptoJS.AES.decrypt(hash, password).toString(CryptoJS.enc.Utf8);
    const createHints = () => {
        const DOMhints = document.querySelector('#hints');
        const HINTS = 
        [
            'Steh immer zu dir',
            'U2FsdGVkX1/HYRh+1DZy+ErQsBdCUogTL/flwJfXVKN0E6R0QHlBDjPK3knyvebH',
            'U2FsdGVkX19M1KAL8zYROd2DCjlRqXYzCANFIxZeh7JFoY8qTsTOkEWjXe4kuUKL',
            '2FsdGVkX1+9PhCUKrLh/Jr4DuLEisY6w9R6AdRo0+nt2OdeERWMHRRM4mOlYoiE',
            'U2FsdGVkX1+oEzibepFtoLrXWcjnNUsalHZ6uymGmZqh/XtMOqjVtQTKswX3JdYpG6KSMlH6SH6oNVzA570NBg==',
            'U2FsdGVkX1+E1+HmOw+SoQTHHiG9Azbtz4iHKp6h0X+j0D1M+gklxN72ndyWEe4+cOq9vge4hD+UADIrmIJ7Cw==',
            'U2FsdGVkX1+wIvUH4v7Tbit8ETzSmnFus9xg2B9FVFx66Sn9HaqhpzUSdevcKROBQ5eC2JmBdizgQSJCdZi8OqrvU9WOVSL8TNIRd1jb4Rwucnn/xoD/m1RDHbfWDiPD',
            'U2FsdGVkX19o2yswwO6JPFLbkQ19mfbvz8sFa3+4Uo3mkfFthC8QvvaLYaCbTusMG/bCgXa6HjFNgyftVok7ew==',
            'U2FsdGVkX1++kkxHXkI3Ndfl2RiLBkOUHUehqcAScr6p/5/8GDfeScbK2hs6XtCUa1J+uVQCLDaymsP7PDtOpA==',
        ];
        let first = true;
        for (const hint of HINTS)
        {
            const DOMhint = document.createElement('tr');
            DOMhint.classList.add('hint');
            DOMhints.appendChild(DOMhint);
            const DOMdisplay = document.createElement('td');
            DOMdisplay.innerText = hint;
            DOMhint.appendChild(DOMdisplay);
            if (first) first = false;
            else
            {
                const DOMkey = document.createElement('td');
                DOMkey.classList.add('key');
                DOMhint.appendChild(DOMkey);
                const DOMinput = document.createElement('input');
                DOMinput.type = 'text';
                DOMinput.classList.add('input');
                DOMinput.onchange = () => 
                {
                    const dec = decrypt(hint, DOMinput.value);
                    if (dec.length > 0) 
                    {
                        DOMdisplay.innerText = dec;
                        DOMinput.classList.add('right');
                        DOMinput.disabled = true;
                    }
                    else 
                    {
                        DOMinput.classList.add('wrong');
                        setTimeout(()=>{DOMinput.classList.remove('wrong')}, 300);
                    }
                };
                DOMkey.appendChild(DOMinput);
            }
        }
        const DOMstartBtn = document.querySelector('#startBtn');
        const DOMstartCLI = document.querySelector('#startCLI');
        DOMstartBtn.addEventListener('click', () => 
        {
            DOMstartBtn.hidden = true;
            DOMstartCLI.hidden = false;
            DOMhints.hidden = false;
        });
    }

    createHints();
}

main();

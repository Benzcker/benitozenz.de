const DOM = 
{
    overview:   document.querySelector('#overview'),
    change:     document.querySelector('#change'),
    stock:      document.querySelector('#stock'),
    about:      document.querySelector('#about'),
    sidebar: 
    {
        overview:   [document.querySelector('#overview-sidebar'), document.querySelector('#overview')],
        change:     [document.querySelector('#change-sidebar'), document.querySelector('#change')],
        stock:      [document.querySelector('#stock-sidebar'), document.querySelector('#stock')],
        about:      [document.querySelector('#about-sidebar'), document.querySelector('#about')],
    }
};
export default DOM;

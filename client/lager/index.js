import DOM from "./DOM.js";

window.onload = () => 
{
    // hide all non-overview-content
    const allContent = [DOM.overview, DOM.stock, DOM.change, DOM.about];
    for (const elem of allContent.filter(c => c != DOM.overview )) elem.hidden = true;
    for (const key of Object.keys(DOM.sidebar)) 
        DOM.sidebar[key][0].onclick = () => 
        {
            for (const elem of allContent) elem.hidden = true;
            DOM.sidebar[key][1].hidden = false;
        };
};

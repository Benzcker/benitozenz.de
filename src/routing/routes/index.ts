import { Router } from 'express';
import Main from '../controllers/Main';
import { isLoggedInFrontend } from '../validators/authJWT'

export default (): Router => 
{
    const router = Router();

    //router.use('/lager', [isLoggedInFrontend], Main.GETlager);
    router.use('/lager', [], Main.GETlager);

    // Host client files
    router.use(Main.clientfiles);

    return router;
}
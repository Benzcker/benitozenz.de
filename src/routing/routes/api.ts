import { Router } from 'express';
import auth from './api/auth';

export default (): Router => {
    const router = Router();

    router.use('/auth', auth());

    return router;
};


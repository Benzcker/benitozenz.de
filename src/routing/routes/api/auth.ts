import { Router } from 'express';
import Auth from '../../controllers/api/Auth';
import { isLoggedIn } from '../../validators/authJWT';
import { validUsernameGiven } from '../../validators/verifyUsername';

export default (): Router => 
{
    const router = Router();

    router.post('/login',                               Auth.POSTlogin);
    router.post('/register',    [ validUsernameGiven ], Auth.POSTregister);
    router.get( '/whoami',      [ isLoggedIn ],         Auth.GETwhoami);

    return router;
};

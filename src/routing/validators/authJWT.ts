import { NextFunction, Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import connectTo from '../../db/connectTo';
import GENERAL_DB from '../../db/databases/general.conf';
import { User } from '../../db/models/User';
import { TokenContent } from '../controllers/api/Auth';

export enum ROLE 
{
    USER        = 'User',
    SUPPORTER   = 'Supporter',
    DEVELOPER   = 'Developer',
}

const SECRET = process.env['TOKEN_SECRET'] || '51b2575f2af79bfdaad787c69cad336683c43ff6b38e475df4d7069280bbcc0c3385219c73e23f7e7fc320e18a4dcaa182ab23188c023b1c95be0a81ac3da675';

const createIsLoggedIn = (isFrontend: boolean) => 
{
    return (req: Request, res: Response, next: NextFunction) => 
    {
        let token = req.headers['x-access-token'] as string | undefined;
        if (!token) 
        {
            if (isFrontend) res.redirect('/login?fwd=' + req.baseUrl);
            else res.status(403).send('Nicht angemeldet!');
            return;
        }

        jwt.verify(token, SECRET, (err, decoded) => 
        {
            try 
            {
                if (err) throw err; 
                req.userId = (decoded as TokenContent).userId; 
            }
            catch (e)
            {
                res.status(401).send('Nicht autorisiert!');
                return;
            }
            next();
        });
    };
};

export const isLoggedInFrontend = createIsLoggedIn(true);
export const isLoggedIn = createIsLoggedIn(false);

const isROLE = (role: ROLE) =>
{
    return async (req: Request, res: Response, next: NextFunction) => 
    {
        try 
        {
            const connection = await connectTo(GENERAL_DB);
            const user = await User.findById(req.userId);
            if (!user) 
            {
                res.status(403).send('Nutzer existiert nicht! Bitte neu anmelden!');
                return;
            }
            const isUser = user.roles.includes(role);
            if (!isUser) 
            {
                res.status(401).send('Nicht autorisiert!');
                return;
            }
            await connection.disconnect();
            next();
        }
        catch (err) 
        {
            res.status(500).send(err);
            return;
        }
    };
}

export const isUser         = isROLE(ROLE.USER);
export const isSupporter    = isROLE(ROLE.SUPPORTER);
export const isDeveloper    = isROLE(ROLE.DEVELOPER);

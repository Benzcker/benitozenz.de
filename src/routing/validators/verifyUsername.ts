import { NextFunction, Request, Response } from "express";
import connectTo from "../../db/connectTo";
import GENERAL_DB from "../../db/databases/general.conf";
import { User } from "../../db/models/User";

export const validUsernameGiven = async (req: Request, res: Response, next: NextFunction) => 
{
    // Check, if a username is given
    const { username } = req.body;
    if (!username || username.length === 0) 
    {
        res.status(400).send('Es muss ein Nutzername angegeben werden!');
        return;
    } 

    // Check, if there is already a user with that name
    const connection = await connectTo(GENERAL_DB);
    const userWithSameName = await User.findOne({name: username});
    await connection.disconnect();

    if (userWithSameName) 
    {
        res.status(400).send('Nutzername ist bereits vergeben!');
        return;
    }

    // Username is valid
    next();
};

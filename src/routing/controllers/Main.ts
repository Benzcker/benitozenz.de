import express, { Request, Response } from 'express';
import path from 'path';

const client = path.join(__dirname, '/../../../client/');
const lagerIndex = path.join(client, '/lager');

export default class Main 
{
    static clientfiles = express.static(client);

    static GETlager = (req: Request, res: Response) => res.sendFile(lagerIndex);
}

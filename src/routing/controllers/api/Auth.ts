import bcrypt from 'bcryptjs';
import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';
import mongoose from 'mongoose';
import connectTo from '../../../db/connectTo';
import GENERAL_DB from '../../../db/databases/general.conf';
import { User } from '../../../db/models/User';
import { ROLE } from '../../validators/authJWT';

const SECRET = process.env['TOKEN_SECRET'] || '51b2575f2af79bfdaad787c69cad336683c43ff6b38e475df4d7069280bbcc0c3385219c73e23f7e7fc320e18a4dcaa182ab23188c023b1c95be0a81ac3da675';
const TOKEN_EXPIRATION = '90d';
export interface TokenContent 
{
    userId: mongoose.Types.ObjectId;
}

export default class Auth 
{
    static async POSTlogin(req: Request, res: Response, next: NextFunction): Promise<void> 
    {
        try 
        {
            // Anmeldedaten extrahieren
            const { 
                username: name,
                password 
            } = req.body;

            // Nutzer in der Datenbank suchen
            const connection = await connectTo(GENERAL_DB);
            const user = await User.findOne({name});
            if (!user) 
            {
                res.status(400).send('Kein Nutzer mit diesem Namen vorhanden!');
                return;
            }
            await connection.disconnect();

            // Passwort überprüfen
            const correctPassword = await bcrypt.compare(password, user.passwordHash);
            if (!correctPassword) 
            {
                res.status(400).send('Falsches Passwort!');
                return;
            }
            
            // JWT Token generieren
            const payload: TokenContent = { userId: user._id };
            const token = jwt.sign(payload, SECRET, 
                {
                    expiresIn: TOKEN_EXPIRATION,
                });

            // Antwort senden
            res.send(
                {
                    userName: user.name,
                    roles: user.roles,
                    accessToken: token,
                });
        } catch (e) 
        {
            console.error(e);
            res.status(500).send('Es ist ein interner Fehler aufgetreten, bitte melde dies an kontakt@benitozenz.de');
        }
    }

    static async POSTregister(req: Request, res: Response, next: NextFunction): Promise<void> 
    {
        try 
        {
            // Anmeldedaten extrahieren
            const { 
                username: name,
                password 
            } = req.body;
            const roles = [ ROLE.USER ];

            const salt = await bcrypt.genSalt();
            const passwordHash = await bcrypt.hash(password, salt);

            // User anlegen in DB
            const connection = await connectTo(GENERAL_DB);
            const user = new User(
            {
                name,
                passwordHash,
                roles,
            });
            await user.save();
            await connection.disconnect();

            const token = jwt.sign({ id: user._id }, SECRET, 
                {
                    expiresIn: TOKEN_EXPIRATION,
                });

            // Antwort senden
            res.send(
                {
                    userName: user.name,
                    roles: user.roles,
                    accessToken: token,
                });
        } catch (e) 
        {
            console.error(e);
            res.status(500).send('Es ist ein interner Fehler aufgetreten, bitte melde dies an kontakt@benitozenz.de');
        }
    }

    static async GETwhoami(req: Request, res: Response, next: NextFunction): Promise<void> 
    {
        try 
        {
            const connection = await connectTo(GENERAL_DB);
            const user = await User.findById(req.userId);
            if (!user) 
            {
                res.status(400).send('Nutzer nicht gefunden!');
                return;
            }
            await connection.disconnect();
            res.send(
                {
                    userName: user.name,
                    roles: user.roles,
                });
        }
        catch (e)
        {
            res.status(500).send(e);
        }
    }
}

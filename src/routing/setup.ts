import { Express } from 'express';
import client from './routes/index';
import api from './routes/api';

export default (app: Express): void => 
{
    app.use('/api', api());
    app.use(client());
}

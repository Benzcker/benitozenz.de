import connectTo from "./connectTo";
import GENERAL_DB from "./databases/general.conf";

const setupDB = async () => 
{
    try 
    {
        const connection = await connectTo(GENERAL_DB);

        console.log('Successful connection to MongoDB!');

        await connection.disconnect();
    }
    catch (e) 
    {
        console.error(e)
    }
};

export default setupDB;

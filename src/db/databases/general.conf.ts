import DB_Conf from "./conf.type";

const GENERAL_DB: DB_Conf = 
{
    HOST:   process.env['MONGO_HOST'] || 'localhost',
    PORT:   27017,
    DB:     'general'
};

export default GENERAL_DB;

type DB_Conf = 
{
    HOST:   String,
    PORT:   number,
    DB:     String
};

export default DB_Conf;

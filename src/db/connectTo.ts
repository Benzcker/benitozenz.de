import { connect, ConnectOptions } from "mongoose";
import DB_Conf from "./databases/conf.type";

const connectTo = ({DB, PORT, HOST}: DB_Conf) => 
{
    let options : ConnectOptions = {};
    const { MONGO_USER, MONGO_PW } = process.env;
    // Use authentication, if MONGO_USER is specified in .env 
    if ( MONGO_USER && MONGO_USER.length > 0 ) options = 
        { auth: 
            {
                username: MONGO_USER,
                password: MONGO_PW,
            }
        };

    return connect(`mongodb://${HOST}:${PORT}/${DB}`, options);
};

export default connectTo;

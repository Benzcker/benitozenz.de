import express, { Express, Request, Response } from 'express';
import bodyParser from 'body-parser';
import path from 'path';
import dotenv from 'dotenv';
dotenv.config({ path: path.join(__dirname, '../config/.env') });

import setupRouting from './routing/setup';
import setupDB from './db/setup';

const main = async (port: number) => 
{
    const app: Express = express();

    // Install middleware
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));

    // Redirect to blog for now
    app.get('*', (_: Request, res: Response) => res.redirect('https://blog.benitozenz.de/'));

    //setupRouting(app);

    //await setupDB();

    app.listen(port, () =>
    {
        console.log(`Server is running on port ${port}.`);
    });
};

const PORT = parseInt(process.env['PORT'] || '5000');

main(PORT);

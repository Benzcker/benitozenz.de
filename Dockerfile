FROM node:16.3-alpine

WORKDIR /usr/share/html

COPY client/ client/
COPY src/ src/
COPY package.json .
COPY tsconfig.json .

RUN npm i

RUN npm run build

EXPOSE 5000

ENV NODE_ENV production

CMD [ "node", "build/index.js" ]
